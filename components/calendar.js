const util = require("calendarUtil.js");
const date = new Date();
Component({
  properties: {
    term: {
      type: String,
      value: '2019-2020-2',
    },
    termStart: {
      type: String,
      value: '2020-02-10',
    },
    weekCount: {
      type: Number,
      value: 29
    },
    // vacationStart: {
    //   type: Number,
    //   value: 24
    // }
    vacationStart: {
      type: String,
      value: "2020-06-21"
    },
    vacationEnd:{
      type:String,
      value:"2020-08-20"
    },
    vocationName:{
      type: String,
      value: "寒假"
    }
  },
  data: {
    calendarData: [],
    allowClick: true,
    vacationDateDiff: 0,
    vacationStartDate: "",
    curMonth: util.formatDate("MM", date),
    curYear: util.formatDate("yyyy", date),
    today: util.formatDate(undefined, date)
  },
  created: function () {
    util.extDate();
  },
  ready: function () {
    this.calcVacation();
    this.redayForDate(date);
  },
  observers:{
    vacationStart(val){
      this.calcVacation();
      this.redayForDate(date);
    }
  },
  methods: {
    jumpDate: function(e){
      var d = new Date(e.currentTarget.dataset.d);
      this.data.curMonth = util.formatDate("MM", d);
      this.data.curYear = util.formatDate("yyyy", d);
      this.redayForDate(d);
      this.setData({
        curMonth: this.data.curMonth,
        curYear: this.data.curYear,
      })
    },
    switchMonth:function(e){
      var d = new Date(this.data.curYear+"-"+this.data.curMonth+"-01");
      var s = e.currentTarget.dataset.s;
      if (s === "l") d.addDate(0,-1);
      else d.addDate(0,1);
      this.data.curMonth = util.formatDate("MM", d);
      this.data.curYear = util.formatDate("yyyy", d);
      this.redayForDate(d);
      this.setData({
        curMonth: this.data.curMonth,
        curYear: this.data.curYear,
      })
    },
    redayForDate: function(date){
      var curMonthDay = util.formatDate("yyyy-MM-01", date);
      var monthStart = new Date(curMonthDay);
      var monthStartWeekDay = monthStart.getDay();
      monthStartWeekDay = monthStartWeekDay === 0 ? 7 : monthStartWeekDay;
      var calendarStart = monthStart;
      calendarStart.addDate(0, 0, -(monthStartWeekDay - 1));
      this.showCalendar(calendarStart);
    },
    getWeekNum(dateString) {
      var da = '';
      if (dateString == undefined) {
        var now = new Date();
        var now_m = now.getMonth() + 1;
        now_m = (now_m < 10) ? '0' + now_m : now_m;
        var now_d = now.getDate();
        now_d = (now_d < 10) ? '0' + now_d : now_d;
        da = now.getFullYear() + '-' + now_m + '-' + now_d;
        console.log('今天系统时间是:' + da);
      } else {
        da = dateString;//日期格式2015-12-30
      }
      var date1 = new Date(da.substring(0, 4), parseInt(da.substring(5, 7)) - 1, da.substring(8, 10));//当前日期
      var date2 = new Date(da.substring(0, 4), 0, 1); //1月1号
      //获取1月1号星期（以周一为第一天，0周一~6周日）
      var dateWeekNum = date2.getDay() - 1;
      if (dateWeekNum < 0) { dateWeekNum = 6; }
      if (dateWeekNum < 4) {
        //前移日期
        date2.setDate(date2.getDate() - dateWeekNum);
      } else {
        //后移日期
        date2.setDate(date2.getDate() + 7 - dateWeekNum);
      }
      var d = Math.round((date1.valueOf() - date2.valueOf()) / 86400000);
      if (d < 0) {
        var date3 = (date1.getFullYear() - 1) + "-12-31";
        return getYearWeek(date3);
      } else {
        //得到年数周数
        var week = Math.ceil((d + 1) / 7);
        return week;
      }
    },
    showCalendar: function (start) {
      var showArr = [];
      let week = this.getWeekNum(this.data.curYear +"-"+ this.data.curMonth+"-01")-1;
      for (let i = 0; i < 6; ++i) {
        let innerArr = [];
        for (let k = 0; k < 7; ++k) {
          let unitDate = util.formatDate("yyyy-MM-dd", start);
          if (k === 0) {
            // week = parseInt((util.dateDiff(this.data.termStart, unitDate) / 7)) + 1;
            // week = week > 0 ? week : 0;
            week++;
            innerArr.push({ day: week, color: "week ", type: "周次", detach:"" })
          }
          let unitObj = { day: unitDate.split("-")[2], color: "notCurMonth ", type: "", detach: ""};
          if (util.formatDate("MM", start) === this.data.curMonth) unitObj.color = "curMonth ";
          if (unitDate === this.data.today) unitObj.color = "today ";
          if (unitDate === this.data.termStart) {
          unitObj.type = "开学"; unitObj.color = "termStart ";}
          if (k === 5 || k === 6) {
            unitObj.type = "周末";
            unitObj.color += "weekend ";
          } else if (unitDate>=this.data.termStart && unitDate<this.data.vacationStart) {/*week && week < this.data.weekCount*/
            var tmpColor = "classes ";
            unitObj.type = "教学"; unitObj.detach = "cdetach";
            if (week >= this.data.vacationStart) { unitObj.type = "假期"; tmpColor = "vacation "; unitObj.detach=""; }
            unitObj.color += tmpColor;
          }
          if (unitDate === this.data.vacationStartDate) {
          unitObj.color = "vacationStart "; unitObj.type = "假期";}
          if (unitDate === this.data.vacationEnd) {
            unitObj.color = "vacationEnd "; unitObj.type = "假期";
          }
          if (unitDate > this.data.vacationStartDate && unitDate<this.data.vacationEnd) { unitObj.color = "longVacation "; unitObj.type = "假期";} 
          if (unitDate == unitDate.split("-")[0]+"-04-04"){
            unitObj.type = "清明节";
            unitObj.color = "rest ";
          }
          innerArr.push(unitObj);
          start.addDate(0, 0, 1);
        }
        showArr.push(innerArr);
      }
      this.setData({
        calendarData: showArr
      })
    },
    calcVacation: function () {
      // var d = new Date(this.data.termStart);
      // d.addDate(0, 0, (this.data.vacationStart - 1) * 7);
      // var vacationStartDate = util.formatDate(undefined, d);
      let vacationStartDate=this.data.vacationStart
      this.setData({ 
        vacationStartDate: vacationStartDate,
        vacationDateDiff: util.dateDiff(this.data.today, vacationStartDate)
      })
    }
  }
})