/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : edu2

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2020-03-25 21:17:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for clazz
-- ----------------------------
DROP TABLE IF EXISTS `clazz`;
CREATE TABLE `clazz` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `clazz_desc` varchar(255) DEFAULT NULL COMMENT '班级描述',
  `clazz_name` varchar(20) DEFAULT NULL COMMENT '班级名称',
  `specialty_id` bigint(20) DEFAULT NULL COMMENT '专业id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='班级信息表';

-- ----------------------------
-- Records of clazz
-- ----------------------------
INSERT INTO `clazz` VALUES ('2', '2020-03-20 04:28:02', '1', '2020-03-20 04:28:02', null, '计应2001', '1');

-- ----------------------------
-- Table structure for college
-- ----------------------------
DROP TABLE IF EXISTS `college`;
CREATE TABLE `college` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `college_desc` varchar(255) DEFAULT NULL COMMENT '学院描述',
  `college_name` varchar(20) DEFAULT NULL COMMENT '学院名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='学院信息表';

-- ----------------------------
-- Records of college
-- ----------------------------
INSERT INTO `college` VALUES ('1', '2020-03-19 09:57:49', '1', '2020-03-19 09:57:49', null, '计算机学院');
INSERT INTO `college` VALUES ('2', '2020-03-19 10:54:08', '1', '2020-03-19 10:54:08', null, '商学院');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `course_desc` varchar(255) DEFAULT NULL COMMENT '课程描述',
  `course_name` varchar(20) DEFAULT NULL COMMENT '课程名称',
  `course_score` float DEFAULT '0' COMMENT '课程学分',
  `course_type` int(11) DEFAULT '1' COMMENT '课程类型 1主修 2限选 3任选 4环节',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='课程信息表';

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '2020-03-22 09:10:28', '1', '2020-03-22 09:10:28', '大学入门推荐课程之一', '大学计算机基础', '3', '1');
INSERT INTO `course` VALUES ('2', '2020-03-22 09:13:14', '1', '2020-03-22 09:13:14', '入学必修课', '毛泽东思想和中国特色社会主义理论体系', '6', '1');

-- ----------------------------
-- Table structure for exam
-- ----------------------------
DROP TABLE IF EXISTS `exam`;
CREATE TABLE `exam` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `clazz_id` bigint(20) DEFAULT NULL COMMENT '班级',
  `course_id` bigint(20) DEFAULT NULL COMMENT '科目',
  `end_year` varchar(20) DEFAULT NULL COMMENT '学年结束year',
  `exam_start_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '考试时间',
  `exam_time` int(11) DEFAULT '120' COMMENT '考试时间，单位为分钟',
  `exam_type` int(11) DEFAULT '1' COMMENT '考试轮次(类型) 1中考 2末考 3补考 4重修考',
  `room` varchar(20) DEFAULT NULL COMMENT '考试地点',
  `start_year` varchar(20) DEFAULT NULL COMMENT '学年开始year',
  `teacher_num` varchar(20) DEFAULT NULL COMMENT '监考老师',
  `term` int(11) DEFAULT '1' COMMENT '学期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='考试安排信息表';

-- ----------------------------
-- Records of exam
-- ----------------------------
INSERT INTO `exam` VALUES ('1', '2020-03-24 03:40:35', '1', '2020-03-24 03:40:35', '2', '1', '2020', '2021-05-24 12:00:00', '60', '1', '第一教学楼201', '2019', '卜存载', '2');
INSERT INTO `exam` VALUES ('2', '2020-03-24 04:54:21', '0', '2020-03-24 04:54:21', '2', '2', '2020', '2020-03-24 09:01:00', '60', '1', 'dadad', '2019', 'ggggg', '2');
INSERT INTO `exam` VALUES ('3', '2020-03-25 11:35:23', '1', '2020-03-25 11:35:23', '2', '1', '2020', '2020-11-26 10:00:00', '60', '4', '实训楼203', '2019', 'lisa', '2');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `content` text COMMENT '公告内容',
  `origin` varchar(32) DEFAULT NULL COMMENT '公共来源',
  `title` varchar(20) DEFAULT NULL COMMENT '公告标题',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='公告信息表';

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('1', '2020-03-18 08:27:49', '1', '2020-03-18 08:27:49', '接教育部通知，这些消息需要及时发布。副书记方式可加入而我UR和肯定会峰会上开发和思考时间段划分是客户反馈我让我看任何未录入了楼复式楼房水位日历IEUR排污染困了就睡打开看了几个UI二维自己会发生教后反思绝代风华二纬路发货单将粉红色双方均会发生。', '教育部', '这是一则公告');
INSERT INTO `notice` VALUES ('2', '2020-03-18 10:09:15', '0', '2020-03-18 10:09:15', '大撒大声地撒大所多撒大所大', 'dasdasd', 'asdas');

-- ----------------------------
-- Table structure for rebuild
-- ----------------------------
DROP TABLE IF EXISTS `rebuild`;
CREATE TABLE `rebuild` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `count` int(11) DEFAULT '1' COMMENT '重修人数',
  `course_id` bigint(20) DEFAULT NULL COMMENT '科目',
  `course_num` int(11) DEFAULT '1' COMMENT '第几节课',
  `end_year` varchar(20) DEFAULT NULL COMMENT '学年结束year',
  `rebuild_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '重修时间',
  `room` varchar(20) DEFAULT NULL COMMENT '上课地点',
  `start_year` varchar(20) DEFAULT NULL COMMENT '学年开始year',
  `stu_num` varchar(20) DEFAULT NULL COMMENT '学生学号',
  `teacher_num` varchar(20) DEFAULT '' COMMENT '代课老师',
  `term` int(11) DEFAULT '1' COMMENT '学期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='重修信息表';

-- ----------------------------
-- Records of rebuild
-- ----------------------------
INSERT INTO `rebuild` VALUES ('4', '2020-03-25 12:13:29', '1', '2020-03-25 12:13:29', '20', '1', '20', '2020', '2020-04-25 09:00:00', 'fs56', '2019', 'stu1001', 'Lisaa', '2');
INSERT INTO `rebuild` VALUES ('5', '2020-03-25 12:17:10', '1', '2020-03-25 12:17:10', '20', '2', '6', '2020', '2020-05-25 09:00:00', 'LE01001', '2019', 'stu1002', '贾老师', '2');
INSERT INTO `rebuild` VALUES ('6', '2020-03-25 12:21:33', '1', '2020-03-25 12:21:33', '30', '2', '20', '2020', '2021-03-25 12:01:00', '一教203', '2019', 'stu1001', 'Jack', '2');

-- ----------------------------
-- Table structure for result
-- ----------------------------
DROP TABLE IF EXISTS `result`;
CREATE TABLE `result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `course_id` bigint(20) DEFAULT NULL COMMENT '课程',
  `end_year` varchar(20) DEFAULT NULL COMMENT '学年结束',
  `exam_method` int(11) DEFAULT '1' COMMENT '考试方式：1考查 2考核',
  `exam_type` int(11) DEFAULT '1' COMMENT '考试轮次(类型) 1中考 2末考 3补考 4重修考',
  `result` int(11) DEFAULT '0' COMMENT '有效成绩',
  `result2` int(11) DEFAULT '0' COMMENT '原始成绩',
  `score` float DEFAULT '0' COMMENT '学分',
  `start_year` varchar(20) DEFAULT NULL COMMENT '学年开始',
  `stu_num` varchar(20) DEFAULT NULL COMMENT '学生学号',
  `term` int(11) DEFAULT '1' COMMENT '学期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='成绩信息表';

-- ----------------------------
-- Records of result
-- ----------------------------
INSERT INTO `result` VALUES ('2', '2020-03-22 09:52:22', '1', '2020-03-22 09:52:22', '1', null, '2', '2', '60', '86', '2', '2019-2020', 'stu1001', '2');
INSERT INTO `result` VALUES ('3', '2020-03-22 10:09:47', '1', '2020-03-22 10:09:47', '2', null, '2', '4', '70', '70', '6', '2019-2020', 'stu1001', '2');

-- ----------------------------
-- Table structure for specialty
-- ----------------------------
DROP TABLE IF EXISTS `specialty`;
CREATE TABLE `specialty` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `college_id` bigint(20) DEFAULT NULL COMMENT '学院id',
  `specialty_desc` varchar(255) DEFAULT NULL COMMENT '专业描述',
  `specialty_name` varchar(20) DEFAULT NULL COMMENT '专业名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='专业信息表';

-- ----------------------------
-- Records of specialty
-- ----------------------------
INSERT INTO `specialty` VALUES ('1', '2020-03-19 11:04:41', '1', '2020-03-19 11:04:41', '1', null, '计算机应用技术');
INSERT INTO `specialty` VALUES ('2', '2020-03-19 12:18:27', '1', '2020-03-19 12:18:27', '2', null, '计算机网络技术');

-- ----------------------------
-- Table structure for teaching
-- ----------------------------
DROP TABLE IF EXISTS `teaching`;
CREATE TABLE `teaching` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `clazz_id` bigint(20) DEFAULT NULL COMMENT '班级',
  `count` int(11) DEFAULT '1' COMMENT '班级人数',
  `course_id` bigint(20) DEFAULT NULL COMMENT '教学科目',
  `end_year` varchar(20) DEFAULT NULL COMMENT '学年结束',
  `name` varchar(32) DEFAULT NULL COMMENT '实验名称(实训名称)',
  `room` varchar(20) DEFAULT NULL COMMENT '实验地点(上课地点、实训地点)',
  `start_year` varchar(20) DEFAULT NULL COMMENT '学年开始',
  `teach_time` int(11) DEFAULT '120' COMMENT '学时',
  `teaching_type` int(11) DEFAULT '1' COMMENT '教学类型 1实验教学 2理论教学 3实训教学',
  `term` int(11) DEFAULT '1' COMMENT '学期',
  `test_type` int(11) DEFAULT '1' COMMENT '实验类型 1验证性 2设计性',
  `weeks` int(11) DEFAULT '1' COMMENT '第几周',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='教学安排信息表';

-- ----------------------------
-- Records of teaching
-- ----------------------------

-- ----------------------------
-- Table structure for timetable
-- ----------------------------
DROP TABLE IF EXISTS `timetable`;
CREATE TABLE `timetable` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `clazz_id` bigint(20) DEFAULT NULL COMMENT '班级id',
  `course_id` bigint(20) DEFAULT NULL COMMENT '课程id',
  `end_year` varchar(20) DEFAULT NULL COMMENT '学年结束year',
  `num` int(11) DEFAULT '1' COMMENT '第几节课',
  `room` varchar(20) DEFAULT NULL COMMENT '上课教室',
  `start_year` varchar(10) DEFAULT NULL COMMENT '学年开始year',
  `teacher_id` bigint(20) DEFAULT NULL COMMENT '任课教师',
  `term` int(11) DEFAULT '1' COMMENT '学期',
  `week` int(11) DEFAULT '1' COMMENT '星期',
  `weeks` int(11) DEFAULT '1' COMMENT '周次',
  `len` int(11) DEFAULT '1' COMMENT '连续上课长度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='课表信息表';

-- ----------------------------
-- Records of timetable
-- ----------------------------
INSERT INTO `timetable` VALUES ('2', '2020-03-23 07:32:00', '1', '2020-03-23 07:32:00', '2', '2', '2020', '1', '第一教学楼', '2019', '8', '2', '1', '13', '4');
INSERT INTO `timetable` VALUES ('3', '2020-03-23 07:34:52', '1', '2020-03-23 07:34:52', '2', '1', '2020', '5', '第二教学楼2201', '2019', '9', '2', '2', '13', '2');
INSERT INTO `timetable` VALUES ('4', '2020-03-23 07:54:47', '1', '2020-03-23 07:54:47', '2', '1', '2020', '5', '逸夫楼110', '2019', '8', '2', '1', '13', '4');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `age` varchar(20) DEFAULT '1' COMMENT '年龄',
  `clazz_id` bigint(20) DEFAULT NULL COMMENT '班级id',
  `edu` varchar(20) DEFAULT NULL COMMENT '教师学历',
  `nat` varchar(20) DEFAULT NULL COMMENT '籍贯',
  `num` varchar(20) DEFAULT NULL COMMENT '学号或职工号',
  `open_id` varchar(64) DEFAULT NULL COMMENT '微信的openID',
  `password` varchar(64) DEFAULT NULL COMMENT '登陆密码',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机',
  `role_id` int(11) DEFAULT '1' COMMENT '角色 1管理员 2教师 3学生 4领导',
  `sex` int(11) DEFAULT '1' COMMENT '性别 1男 0女',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '入学时间',
  `style` varchar(20) DEFAULT NULL COMMENT '教师练习方式',
  `title` varchar(20) DEFAULT '0' COMMENT '教师职称 1讲师、2教授、3助教、4副教授',
  `username` varchar(20) DEFAULT NULL COMMENT '姓名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_num` (`num`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表';

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('1', '2020-03-20 07:59:31', '1', '2020-03-20 10:43:33', '22', '2', null, '湖北', 'stu1001', null, 'e10adc3949ba59abbe56e057f20f883e', '13123456789', '3', '1', '2019-09-17 16:00:00', null, null, 'binggan');
INSERT INTO `user_info` VALUES ('5', '2020-03-20 09:52:08', '1', '2020-03-20 10:28:56', '12', '2', null, '广东', 'stu1002', null, 'e3ceb5881a0a1fdaad01296d7554868d', '13111111111', '3', '0', '2020-03-19 16:00:00', null, null, '小红');
INSERT INTO `user_info` VALUES ('7', '2020-03-20 10:38:22', '1', '2020-03-20 10:43:15', '11', '2', null, '云南', 'stu1003', null, '96e79218965eb72c92a549dd5a330112', '13111111111', '3', '1', '2020-03-19 16:00:00', null, null, '测试学员');
INSERT INTO `user_info` VALUES ('8', '2020-03-20 12:13:25', '1', '2020-03-20 12:32:18', '22', null, '高中', null, 'tea1001', null, 'e10adc3949ba59abbe56e057f20f883e', null, '2', '0', null, '13111111111', '1', '李老师');
INSERT INTO `user_info` VALUES ('9', '2020-03-20 12:36:27', '1', '2020-03-20 12:36:27', '33', null, '本科', null, 'tea1002', null, '1a100d2c0dab19c4430e7d73762b3423', null, '2', '1', null, '1323143432', '3', '测试老师');
INSERT INTO `user_info` VALUES ('10', '2020-03-20 12:39:29', '1', '2020-03-20 12:39:29', null, null, null, null, 'admin1001', null, 'e10adc3949ba59abbe56e057f20f883e', null, '1', '0', null, null, null, '管理员');
INSERT INTO `user_info` VALUES ('11', '2020-03-22 03:34:04', '1', '2020-03-22 03:34:04', null, null, null, null, 'admin1002', null, '21218cca77804d2ba1922c33e0151105', null, '1', '0', null, null, null, '管理员测试号');

-- ----------------------------
-- Table structure for vacation
-- ----------------------------
DROP TABLE IF EXISTS `vacation`;
CREATE TABLE `vacation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `end_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '假期结束时间',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '假期开始时间',
  `vacation_name` varchar(20) DEFAULT NULL COMMENT '假期名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='假期信息表';

-- ----------------------------
-- Records of vacation
-- ----------------------------
INSERT INTO `vacation` VALUES ('2', '2020-03-21 06:18:04', '1', '2020-03-21 06:18:04', '2020-03-20 16:00:00', '2020-01-20 16:00:00', '2020寒假');
INSERT INTO `vacation` VALUES ('5', '2020-03-21 06:37:15', '1', '2020-03-21 06:37:15', '2020-08-21 16:00:00', '2020-08-14 16:00:00', '2020中秋节');
INSERT INTO `vacation` VALUES ('6', '2020-03-21 06:37:29', '0', '2020-03-21 06:37:29', null, '2020-03-20 16:00:00', '打算打');
INSERT INTO `vacation` VALUES ('9', '2020-03-21 09:55:54', '1', '2020-03-21 09:55:54', '2020-08-30 16:00:00', '2020-06-20 16:00:00', '2020暑假');
INSERT INTO `vacation` VALUES ('10', '2020-03-21 12:21:32', '1', '2020-03-21 12:21:32', '2020-08-31 16:00:00', '2020-08-31 16:00:00', '2020开学');

-- ----------------------------
-- Table structure for warning
-- ----------------------------
DROP TABLE IF EXISTS `warning`;
CREATE TABLE `warning` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常 0：删除',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `course_num` int(11) DEFAULT NULL COMMENT '课程环节数',
  `course_type` int(11) DEFAULT '1' COMMENT '课程类型 1主修 2限选 3任选 4环节',
  `have_score` float DEFAULT NULL COMMENT '当前已修学分',
  `needed_score` float DEFAULT NULL COMMENT '当前应修学分',
  `not_have_score` float DEFAULT NULL COMMENT '当前未来获得学分',
  `num` varchar(20) DEFAULT NULL COMMENT '学号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='学情警示信息表';

-- ----------------------------
-- Records of warning
-- ----------------------------
INSERT INTO `warning` VALUES ('1', '2020-03-24 06:28:10', '1', '2020-03-24 06:28:10', '50', '1', '174.5', '174.5', '0', 'stu1001');
INSERT INTO `warning` VALUES ('2', '2020-03-24 07:35:28', '1', '2020-03-24 07:35:28', '10', '2', '13', '13', '0', 'stu1001');
INSERT INTO `warning` VALUES ('5', '2020-03-24 11:40:17', '1', '2020-03-24 11:40:17', '7', '3', '10', null, null, 'stu1001');
