//app.js
App({
  onLaunch: function() {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    wx.getStorage({
      key: 'userInfo',
      success: (res)=>{
        console.log(res)
        this.globalData.localuser = res.data;
        this.globalData.roleId = res.data.roleId;
        this.globalData.userId = res.data.id;
      },
    })
    // this.getOpenid();

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
    // 获取系统状态栏信息
    wx.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight;
        let custom = wx.getMenuButtonBoundingClientRect();
        this.globalData.Custom = custom;
        this.globalData.CustomBar = custom.bottom + custom.top - e.statusBarHeight;
      }
    })
  },
  // 获取用户openid
  getOpenid: function (cb,ecb) {
    // 登录
    let _this = this;
    wx.login({
      success: res => {
        console.log(res);
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.code) {
          wx.request({
            url: this.globalData.baseService +'/wx/getOpenId',
            data: {
              jsCode: res.code
            },
            success(userRes) {
              console.log("拿到的sessionkey", userRes);
              if (userres.data.errorCode == 0) {
                wx.setStorage({
                  key: "openData",
                  data: JSON.stringify(userRes.data)
                });
                _this.globalData.openId = userRes.data.data.openId;
                console.log('globalData', _this.globalData);
                // wx.request({
                //   url: _this.globalData.baseService+'/user/loginByOpenId',
                //   method:"post",
                //   data: { openId: userRes.data.data.openId },
                //   success(getUserRes) {
                //     console.log("getUserRes", getUserRes);
                //     if (getUserRes.data.code === "1") {
                //       _this.globalData.userId = getUserRes.data.data.id;
                //       _this.globalData.roleId = getUserRes.data.data.roleId;
                //       _this.globalData.localuser = getUserRes.data.data
                //       cb && cb(getUserRes.data.data)
                //     }
                //   }
                // })
                // _this.getUserInfo();
              } else {
                wx.showToast({
                  title: userRes.description,
                  icon: 'none',
                  duration: 2000
                })
                ecb && ecb();
              }
            },
            fail() {
              wx.showToast({
                title: "请求出错，请稍后重试！",
                icon: 'none',
                duration: 2500
              })
              ecb && ecb();
            }
          })
        }

      }
    })
  },
  getUserInfo() {
    wx.getUserInfo({
      success: res => {
        this.globalData.userInfo = res.userInfo;
        let userInfo = res.userInfo
        wx.getStorage({
          key: 'openData',
          success: function (ores) {
            console.log(ores)
            let tempOpenData = JSON.parse(ores.data);
            //保存用户信息
          }
        })
      }
    });
  },
  getAllClass(cb) {
    //获取所有班级
    wx.request({
      url: this.globalData.baseService + '/clazz/selAll',
      success: (res) => {
        console.log("结果", res);
        if (res.data.errorCode == 0) {
          cb && cb(res.data.data)
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  globalData: {
    author:"binggan",
    roleObj: [{ roleId: 1, text: "管理员" }, { roleId: 2, text: "教员" }, { roleId: 3, text: "学员" }],
    teacherTit:["讲师", "教授", "助教", "副教授"],
    examMethod: ["考查","考核"],
    examType: ["中考", "末考", "补考","重修考"],
    userInfo: null,
    localuser: null,
    openId: "",
    userId: "",
    roleId:null,
    baseService: "http://localhost:8081"
  }
})