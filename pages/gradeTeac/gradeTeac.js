// pages/gradeTeac/gradeTeac.js
const app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: null,
    courseData:[],
    scoreData: [],
    examMethodIndex:null,
    examTypeIndex:null,
    courseIdIndex:null,
    examMethod:[],
    examType:[],
    showModal:false,
    enterScore:null,
    selectClass: { index: null, data: null },
    curScoreData:{},
    selectEduIndex: null,
    examData:[],
    examDetailData:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      examMethod: app.globalData.examMethod,
      examType: app.globalData.examType
    })
    this.getAllCourse();
    // app.getAllClass((data) => {
    //   console.log(data)
    //   this.setData({
    //     classData: data
    //   })
    // })
    this.getAllStu()
  },
// functions
  getAllCourse() {
    //获取所有课程
    wx.request({
      url: app.globalData.baseService + '/course/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.setData({
            courseData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  pickerChange(e){
    let key = e.currentTarget.dataset.key,
      pickerKey = key+"Index",
      objKey = "curScoreData." + key;
    console.log(objKey)
    //日期选择
    this.setData({
      [pickerKey]: e.detail.value,
      [objKey]: +e.detail.value + 1
    })
  },
  getAllEdu() {
    wx.request({
      url: app.globalData.baseService + '/examination/selAllExamination',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          // this.getAllEdu();
          this.setData({
            classData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        // this.hideModal();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  getExamDetail() {
    wx.request({
      url: app.globalData.baseService + '/examDetail/queryByExaminationIdAndClassIdAndIsDeleted',
      data: { classId: this.data.classData[this.data.selectClass.index].id, examinationId: this.data.examData[this.data.selectEduIndex].id },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          let showResData = res.data.data.filter(item => this.data.classData[this.data.selectClass.index].id === item.classId);
          this.setData({
            examDetailData: showResData
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  getAllStu(){
    //查找学生score
    wx.request({
      url: app.globalData.baseService + '/result/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          // this.getAllEdu();
          this.setData({
            scoreData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        // this.hideModal();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  enterScore(){
    //录入成绩
    this.setData({
      showModal:true,
      curScoreData:{}
    })

  },
  hideModal(){
    this.setData({
      showModal: false
    })
  },
  confirmEnter(){
    // 确认录入
    let dataset = JSON.parse(JSON.stringify(this.data.curScoreData));
    dataset.courseId=this.data.courseData[dataset.courseId-1].id;
    wx.request({
      url: app.globalData.baseService + '/result/add',
      method: "post",
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      data: dataset,
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.getAllStu();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        this.hideModal();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
    this.hideModal();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})