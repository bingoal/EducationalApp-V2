// pages/teaTaskManage/teaTaskManage.js
const app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    courseData: [],
    showModal: false,
    showModalDel:false,
    isAddCourse: false,
    curCourseData: {},
    index:null,
    delId:null,
    statusText: ["必修", "限选","任选"]
  },
  // methods
  typeChange(e) {
    let key = "index",
      objKey = "curCourseData.courseType";
    console.log(objKey)
    //日期选择
    this.setData({
      [key]: e.detail.value,
      [objKey]: +e.detail.value + 1
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getAllCourse();
  },
  // functions
  getAllCourse(){
    //获取所有课程
    wx.request({
      url: app.globalData.baseService + '/course/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
            this.setData({
              courseData:res.data.data
            })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  toEditCourse(e) {
    //编辑用户信息
    console.log(e)
    let tempObj = JSON.parse(JSON.stringify(e.currentTarget.dataset.course));
    this.setData({
      showModal: true,
      isAddCourse: false,
      curCourseData: tempObj
    })
  },
  toDelCourse(e){
    this.setData({
      showModalDel:true,
      delId: e.currentTarget.dataset.course
    })
  },
  addCourse() {
    // 新增用户
    this.setData({
      showModal: true,
      isAddCourse: true
    })
  },
  hideModal() {
    this.setData({
      showModal: false,
      curCourseData: {}
    })
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  confirmEnter() {
    //确认编辑
    console.log(this.data.curCourseData)
    wx.request({
      url: app.globalData.baseService + '/course/add', //http://www.hotant.top
      method: 'POST',
      data: this.data.curCourseData,
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.getAllCourse();
          this.hideModal();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  confirmDel() {
    //确认删除
    let delId = this.data.delId;
    wx.request({
      url: app.globalData.baseService + '/course/del',
      data: { id: delId },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          wx.showToast({
            title: "删除成功",
            icon: 'success',
            duration: 2000
          })
          this.getAllCourse();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        this.hideModalDel();
      }
    })
  },
  hideModalDel() {
    this.setData({
      showModalDel: false
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})