// pages/warningManage/warningManage.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    collegeIndex: null,
    specialtyIndex: null,
    index:null,
    curUserData:[],
    collegeData: [],
    specialtyData: [],
    allWarnData: [],
    warnData: [],
    sexName: ["女", "男"],
    courseType: ["必修", "限选", "任选", "环节"],
    showModal: false,
    showModalAdd:false,
    leaningData: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getAllCollege();
    this.getAllWarn();
    this.setData({
      courseType: app.globalData.courseType
    })
  },
  //methods
  typeChange(e) {
    let key = "index",
      objKey = "curUserData.courseType";
    console.log(objKey)
    //日期选择
    this.setData({
      [key]: e.detail.value,
      [objKey]: +e.detail.value + 1
    })
  },
  hideModal() {
    this.setData({
      showModal: false
    })
  },
  hideModalAdd() {
    this.setData({
      showModalAdd: false
    })
  },
  getUnicData(originData) {
    let res = {},
      resArr = [];
    originData.filter(item => {
      let key = item.userInfoDTO.id;
      if (!res[key]) {
        res[key] = true
        resArr.push(item)
      }
    })
    return resArr
  },
  collegeChange(e) {
    let key = e.currentTarget.dataset.key,
      pickerKey = key + "Index";
    this.getSpeByCollege(this.data.collegeData[e.detail.value].id);
    this.setData({
      [pickerKey]: e.detail.value,
      specialtyIndex:null
    })
  },
  specChange(e) {
    let key = e.currentTarget.dataset.key,
      pickerKey = key + "Index";
    this.setData({
      [pickerKey]: e.detail.value,
    })
    let tempUnicData = this.getUnicData(this.data.allWarnData);
    let oData = tempUnicData.filter(item=>item.userInfoDTO.specialtyName == this.data.specialtyData[e.detail.value].specialtyName)
    this.setData({
      warnData: oData
    })
  },
  getAllCollege() {
    //获取所有学院
    wx.request({
      url: app.globalData.baseService + "/college/selAll",
      success: (res) => {
        if (res.data.errorCode == 0) {
          this.setData({
            collegeData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  getSpeByCollege(id) {
    wx.request({
      url: app.globalData.baseService + "/specialty/selAllUnderOneSpecialty",
      data: {
        collegeId: id
      },
      success: (res) => {
        if (res.data.errorCode == 0) {
          this.setData({
            specialtyData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  getAllWarn() {
    //获取所有学情警示
    wx.request({
      url: app.globalData.baseService + "/warning/selAll",
      success: (res) => {
        if (res.data.errorCode == 0) {
          this.setData({
            allWarnData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  getDatasByNum(num){
    wx.request({
      url: app.globalData.baseService + "/warning/selByNum",
      data: { num},
      success: (res) => {
        if (res.data.errorCode == 0) {
          this.setData({
            leaningData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  toReadMore(e) {
    let num = e.currentTarget.dataset.item;
    let data = this.data.allWarnData.filter(item => item.userInfoDTO.num==num)
    this.setData({
      showModal: true,
      leaningData: data
    })
    // this.getDatasByNum(num)
  },
  addCourse(){
    this.setData({
      showModalAdd:true,
      index:null,
      curUserData:{}
    })
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
    if (name == "curUserData.haveScore" || name =="curUserData.neededScore"){
      this.setData({
        "curUserData.notHaveScore": this.data.curUserData.neededScore - this.data.curUserData.haveScore
      })
    }
  },
  confirmEnter(){
    console.log(this.data.curUserData);
    wx.request({
      url: app.globalData.baseService + "/warning/add", //http://www.hotant.top
      method: 'POST',
      data: this.data.curUserData,
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          //更新列表
          this.getAllWarn();
          this.hideModalAdd();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})