// pages/grade/grade.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userId:null,
    examData:[],
    scoreData: [],
    examMethod:[],
    examType:[],
    selectEduIndex:null,//选中的教务索引
    index:null,//选中的考试安排详情
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      examMethod:app.globalData.examMethod,
      examType: app.globalData.examType,
      userId: app.globalData.userId
    })
    this.getScores()
  },
  // functions
  getScores() {
    //查找学生score
    wx.request({
      url: app.globalData.baseService + '/result/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          // this.getAllEdu();
          let oRes=res.data.data;
          let resData=oRes.filter(item => item.student.id==this.data.userId)
          this.setData({
            scoreData: resData
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        // this.hideModal();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})