// pages/chooseCourse/chooseCourse.js
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    courseData: [],
    bgcolor: ['#F8D800', '#0396ff', '#ea5455', '#7367f0', '#32ccbc', '#28c76f', '#d2f1f0', '#e7ebed'],
    showModal:false,
    selectItem:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAllElectCourse();
  },
// functions
  getAllElectCourse(){
    //获取所有选修课
    wx.request({
      url: app.globalData.baseService + '/course/selAllElectiveCourse',
      success: (res) => {
        console.log("结果", res);
        if (res.data.errorCode == 0) {
         this.setData({
           courseData:res.data.data
         })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  chooseCourse(e){
    //选课
    this.setData({
      selectItem: e.currentTarget.dataset.item,
      showModal:true
    })
  },
  hideModal() {
    this.setData({
      showModal: false,
      selectItem:null
    })
  },
  confirmEnter(){
    wx.request({
      url: app.globalData.baseService + '/courseSelection/addOrUpdateCourseSelection',
      method: 'POST',
      data: { courserId: this.data.selectItem.id, stuNum: app.globalData.localuser.num},
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        if (res.data.errorCode == 0) {
          wx.showToast({
            title: "已成功选课！",
            icon: 'success',
            duration: 2500
          })
          this.hideModal();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})