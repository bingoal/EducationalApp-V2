// pages/educationManage/educationManage.js
const util = require('../../utils/util.js')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    role: false,
    TabCur: 0,
    examData: [],
    lengthArr: [0, 0],
    classData: [],
    allCourse: [],
    examDetailData: [],
    showModal: false,
    showModalDel: false,
    isAddCourse: false,
    eduIndex: null,
    yearIndex: 1,
    selectClass: {
      index: null,
      data: null
    },
    selectClassIndex: null,
    selectEduIndex: null,
    selCourseIndex: null,
    examStartDate: null,
    date: null,
    time: null,
    testTypeIndex: null,
    examType: [],
    allTeachingData: [],
    teachingList:[],
    yearsData: [{
      name: "2019-2020学年第一学期",
      arr: [2019, 2020, 1]
    }, {
      name: "2019-2020学年第二学期",
      arr: [2019, 2020, 2]
    }, {
      name: "2020-2021学年第一学期",
      arr: [2020, 2021, 1]
    }, {
      name: "2020-2021学年第二学期",
      arr: [2020, 2021, 2]
    }],
    testType: ["验证性", "设计性"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      role: options.role == "admin" ? true : false
    })
    this.getCurDate();
    this.getAllTeaching();
    this.setData({
      examType: app.globalData.examType
    })
    app.getAllClass((data) => {
      console.log(data)
      this.setData({
        classData: data
      })
    })
    this.getAllCourse();
  },
  //functions
  yearChange(e) {
    this.setData({
      "yearIndex": e.detail.value
    })
    this.filterTeachingData();
  },
  pickerChange(e) {
    let key = e.currentTarget.dataset.key,
      pickerKey = key + "Index",
      objKey = "curCourseData." + key;
    console.log(objKey)
    //轮次选择
    this.setData({
      [pickerKey]: e.detail.value,
      [objKey]: +e.detail.value + 1
    })
  },
  classChange(e) {
    console.log(e.detail.value)
    this.setData({
      selectClassIndex: e.detail.value,
      "curCourseData.clazzId": this.data.classData[e.detail.value].id
    })
    console.log(this.data.classData[e.detail.value])
    this.filterTeachingData();
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id
    })
    this.getAllTeaching();
  },
  getCurDate() {
    let date = new Date();
    let year = date.getFullYear(),
      month = (+date.getMonth()) > 8 ? +date.getMonth() + 1 : "0" + (+date.getMonth() + 1),
      day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
    this.setData({
      examStartDate: year + '-' + month + '-' + day
    })
  },
  getResultsByCondition() {
    //TODO
  },
  getCurFormatTime() {
    return util.formatTime(new Date())
  },
  getAllTeaching() {
    wx.request({
      url: app.globalData.baseService + '/teaching/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.setData({
            allTeachingData: res.data.data
          })
          this.filterTeachingData();
          // this.getAllEdu();
          // let oData = res.data.data || [];
          // let tempArr = [0, 0];
          // let curTime = this.getCurFormatTime();
          // console.log("当前时间" + curTime)
          // oData.filter(item => {
          //   if (item.examStartTime > curTime) {
          //     tempArr[0] += 1
          //   } else {
          //     tempArr[1] += 1;
          //   }
          // })
          // let readyData = [];
          // oData.filter(item => {
          //   if (this.data.TabCur == 0 && item.examStartTime > curTime) {
          //     readyData.push(item)
          //   } else if (this.data.TabCur == 1 && item.examStartTime <= curTime) {
          //     readyData.push(item)
          //   }
          // })
          // this.setData({
          //   "examData": readyData,
          //   "lengthArr": tempArr
          // })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        // this.hideModal();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  filterTeachingData() {
    let res = [],
      yearArr = this.data.yearsData[this.data.yearIndex].arr;
    this.data.allTeachingData.filter(item => {
      if (item.teachType == +this.data.TabCur + 1 && item.startYear == yearArr[0] && item.endYear == yearArr[1] && item.term == yearArr[2]) {
        let classIndex = this.data.selectClassIndex;
        if (classIndex!=null && classIndex + "" && item.clazzId == this.data.classData[classIndex].id) {
          res.push(item)
        }
      }
    })
    console.log(res)
    this.setData({
      teachingList:res
    })
  },
  getAllCourse() {
    //获取所有课程
    wx.request({
      url: app.globalData.baseService + '/course/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.setData({
            allCourse: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  getExamDetail() {
    wx.request({
      url: app.globalData.baseService + '/examDetail/queryByExaminationIdAndClassIdAndIsDeleted',
      data: {
        classId: this.data.classData[this.data.selectClass.index].id,
        examinationId: this.data.examData[this.data.selectEduIndex].id
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.setData({
            examDetailData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        this.hideModalDel();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  // classChange(e) {
  //   console.log(e.detail.value)
  //   this.setData({
  //     "selectClass.index": e.detail.value,
  //     "selectClass.data": this.data.classData[e.detail.value]
  //   })
  // },
  eduChange(e) {
    this.setData({
      "selectEduIndex": e.detail.value
    })
    this.getExamDetail();
  },
  selectCourse(e) {
    //选择课程
    this.setData({
      selCourseIndex: e.detail.value,
      "curCourseData.courseId": this.data.allCourse[e.detail.value].id
    })
  },
  DateChange(e) {
    //日期选择
    this.setData({
      date: e.detail.value
    })
  },
  TimeChange(e) {
    //日期选择
    this.setData({
      time: e.detail.value
    })
  },
  addCourse() {
    // 新增用户
    // if (!this.data.selectClass.index || !this.data.selectEduIndex){
    //   wx.showToast({
    //     title: '请先选择班级和教务安排',
    //     icon: 'none',
    //     duration: 2000
    //   })
    //   return false;
    // }
    this.setData({
      showModal: true,
      isAddCourse: true,
      testTypeIndex: null,
      selCourseIndex: null,
      date: null,
      time: null,
    })
  },
  toDeleteCourse(e) {
    // 删除班级
    this.setData({
      showModalDel: true,
      delId: e.currentTarget.dataset.delid
    })
  },
  hideModal() {
    this.setData({
      showModal: false,
      curCourseData: {}
    })
  },
  hideModalDel() {
    this.setData({
      showModalDel: false
    })
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  confirmEnter() {
    //确认编辑
    console.log(this.data.curCourseData)
    let yearArr = this.data.yearsData[this.data.yearIndex].arr;
    // let yearArr=this.data.curCourseData.startEndYear.split("-");
    let dataset = { ...this.data.curCourseData,
      clazzId: this.data.classData[this.data.selectClassIndex].id,
      teachType: this.data.TabCur + 1,
      startYear: yearArr[0],
      endYear: yearArr[1],
      term: yearArr[2]
    };
    if (this.data.TabCur == 0) {
      dataset.testType = +this.data.testTypeIndex + 1;
    }
    // Object.assign(dataset,tempObj, this.data.curCourseData)
    console.log(dataset);
    wx.request({
      url: app.globalData.baseService + '/teaching/add',
      method: "post",
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      data: dataset,
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.getAllTeaching();
          this.hideModal();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  confirmDel() {
    //确认删除
    console.log(this.data.delId)
    wx.request({
      url: app.globalData.baseService + '/teaching/del',
      data: {
        id: this.data.delId
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          wx.showToast({
            title: "删除成功",
            icon: 'success',
            duration: 2000
          })
          this.getAllTeaching();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        this.hideModalDel();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})