// pages/mine/mine.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defalutAvatar: '/images/avatar_default.png',
    userInfo: null,
    localUser: null,
    teacherTit: [],
    roleId: app.globalData.roleId,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    role: ['教务管理员', '教师', '学员']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  exit() {
    wx.removeStorage({
      key: "userInfo",
      success(res) {
        app.globalData.localuser = null;
        app.globalData.roleId = null;
        app.globalData.userId = null;
        wx.switchTab({
          url: '/pages/index/index'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(app.globalData)
    this.setData({
      userInfo: app.globalData.userInfo,
      localUser: app.globalData.localuser,
      roleId: app.globalData.roleId,
      teacherTit: app.globalData.teacherTit
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})