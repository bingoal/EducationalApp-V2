// pages/learning/learning.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    courseType: ["必修", "限选", "任选", "环节"],
    // allWarnData: [],
    leaningData: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAllWarn();
  },
  // methods
  getAllWarn() {
    //获取所有学情警示
    wx.request({
      url: app.globalData.baseService + "/warning/selAll",
      success: (res) => {
        if (res.data.errorCode == 0) {
          // this.setData({
          //   allWarnData: res.data.data
          // });
          // let tempUnicData = this.getUnicData(res.data.data);
          // let oData = tempUnicData.filter(item => item.userInfoDTO.specialtyName == this.data.specialtyData[e.detail.value].specialtyName)
          // this.setData({
          //   warnData: oData
          // })
          let data = res.data.data.filter(item => item.userInfoDTO.num == app.globalData.localuser.num)
          this.setData({
            showModal: true,
            leaningData: data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})