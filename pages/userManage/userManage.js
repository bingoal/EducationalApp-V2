// pages/userManage/userManage.js
//获取应用实例
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabCur: 0,
    stuData: [],
    teaData: [],
    sexName:["女","男"],
    teaTit:[],
    titleVal:null,
    adminData: [],
    showModal: false,
    isAddUser: false,
    index: null,
    selectClassIndex:null,
    classData: [], //班级
    curUserData: {},
    startDate: null,
    startTime: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getCurDate();
    this.getAllUser();
    this.setData({
      teaTit: app.globalData.teacherTit
    })
    //获取所有class
    app.getAllClass((data) => {
      console.log(data)
      this.setData({
        classData: data
      })
    })
  },
  // functions
  getCurDate() {
    let date = new Date();
    let year = date.getFullYear(),
      month = (+date.getMonth()) > 8 ? +date.getMonth() + 1 : "0" + (+date.getMonth() + 1),
      day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
    this.setData({
      startDate: year + '-' + month + '-' + day
    })
  },
  sexChange(e) {
    //选择性别
    this.setData({
      "curUserData.sex": +e.detail.value
    })
  },
  DateChange(e) {
    //日期选择
    this.setData({
      startTime: e.detail.value,
      "curUserData.startTime": e.detail.value+" 00:00:00"
    })
  },
  titleChange(e){
    //职称选择
    this.setData({
      titleVal: e.detail.value,
      "curUserData.title": +e.detail.value+1
    })
  },
  toEditUser(e) {
    //编辑用户信息
    console.log(e)
    let tempObj = JSON.parse(JSON.stringify(e.currentTarget.dataset.user));
    let oIndex=null;
    if (this.data.tabCur == 0) {
      this.data.classData.forEach((item, index) => {
        if (item.id === tempObj.clazzId) oIndex = index;
      })
      this.setData({
        startTime: tempObj.startTime,
        selectClassIndex: oIndex
      })
    }else if(this.data.tabCur==1){
      this.setData({
        titleVal:tempObj.title-1
      })
    }
    this.setData({
      showModal: true,
      isAddUser: false,
      curUserData: tempObj,     
    })
  },
  addUser() {
    // 新增用户
    this.setData({
      showModal: true,
      isAddUser: true,
      curUserData: {},
      selectClassIndex:null,
      titleVal:null,
      startTime:null
    })
  },
  getAllUser(){
    wx.request({
      url: app.globalData.baseService+'/user/selAll',
      success: (res) => {
        console.log("结果", res);
        if (res.data.errorCode == 0) {
          let oRes=res.data.data;
          let tempStuData=oRes.filter(item=>item.roleId==3),
            tempTeaData = oRes.filter(item => item.roleId == 2),
            tempAdminData = oRes.filter(item => item.roleId == 1);
          this.setData({
            stuData: tempStuData,
            teaData: tempTeaData,
            adminData: tempAdminData
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
    })
  },
  getStuByClass() {
    //获取班级下的学生
    wx.request({
      url: app.globalData.baseService + '/student/selByClassId',
      data: { clazzId: this.data.classData[this.data.selectClassIndex].id},
      success: (res) => {
        console.log("结果", res);
        if (res.data.errorCode == 0) {
          this.setData({
            stuData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  getClassStu(e) {
    this.classChange(e);
    this.getStuByClass();
  },
  classChange(e) {
    console.log(e.detail.value)
    this.setData({
      selectClassIndex: e.detail.value,
      "curUserData.clazzId": this.data.classData[e.detail.value].id
    })
    console.log(this.data.classData[e.detail.value])
  },
  hideModal() {
    this.setData({
      showModal: false,
      curUserData: {}
    })
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  confirmEnter() {
    //确认编辑
    console.log(this.data.curUserData);
    let url = this.data.isAddUser?'/user/add':'/user/upd';
    this.setData({
      "curUserData.roleId": this.data.tabCur == 0 ? 3 : (this.data.tabCur == 1 ? 2 : 1),
      "curUserData.sex": this.data.curUserData.sex || 0
    })
    wx.request({
      url: app.globalData.baseService + url, //http://www.hotant.top
      method: 'POST',
      data: this.data.curUserData,
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          //更新教师和管理员列表
          this.getAllUser();
          this.hideModal();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },
  tabSelect(e) {
    this.setData({
      tabCur: e.currentTarget.dataset.id
    });
    // if (this.data.tabCur > 0) {
    //   this.getAllTeacAndAdmin();
    // }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})