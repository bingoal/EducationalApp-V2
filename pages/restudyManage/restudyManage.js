// pages/restudyManage/restudyManage.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    role: false,
    yearIndex: 1,
    collegeIndex: null,
    specialtyIndex: null,
    selCourseIndex: null,
    examStartDate: null,
    selTeacherIndex: null,
    date: null,
    time: null,
    sexName: ["女", "男"],
    yearsData: [{
      name: "2019-2020学年第一学期",
      arr: [2019, 2020, 1]
    }, {
      name: "2019-2020学年第二学期",
      arr: [2019, 2020, 2]
    }, {
      name: "2020-2021学年第一学期",
      arr: [2020, 2021, 1]
    }, {
      name: "2020-2021学年第二学期",
      arr: [2020, 2021, 2]
    }],
    collegeData: [],
    showModalAdd: false,
    showModalDel: false,
    curPostData: {},
    allCourse: [],
    allTeacher: [],
    allListData: [],
    listData: [],
    detailData: {},
    delId: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      role: options.role == "admin" ? true : false,
    })
    this.getAllCollege();
    this.getAllCourse();
    this.getCurDate();
    // this.getAllTeacher()
    this.getResultsByCondition((data)=>{
      if (options.role !== "admin"){
        this.filterListData(app.globalData.localuser.specialtyName)
      }
    });
  },
  //methods
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  yearChange(e) {
    this.setData({
      "yearIndex": e.detail.value
    })
    this.getResultsByCondition();
  },
  collegeChange(e) {
    let key = e.currentTarget.dataset.key,
      pickerKey = key + "Index";
    this.getSpeByCollege(this.data.collegeData[e.detail.value].id);
    this.setData({
      [pickerKey]: e.detail.value,
      specialtyIndex: null
    })
  },
  specChange(e) {
    let key = e.currentTarget.dataset.key,
      pickerKey = key + "Index";
    this.setData({
      [pickerKey]: e.detail.value,
    })
    let filterData = this.data.allListData.filter(item => item.student.specialtyName == this.data.specialtyData[e.detail.value].specialtyName);
    this.setData({
      listData: filterData
    })
    // let tempUnicData = this.getUnicData(this.data.allWarnData);
    // let oData = tempUnicData.filter(item => item.userInfoDTO.specialtyName == this.data.specialtyData[e.detail.value].specialtyName)
    // this.setData({
    //   warnData: oData
    // })
  },
  filterListData(specialtyName){
    let filterData = this.data.allListData.filter(item => item.student.specialtyName == specialtyName);
    this.setData({
      listData: filterData
    })
  },
  selectCourse(e) {
    //选择课程
    this.setData({
      selCourseIndex: e.detail.value,
      "curPostData.courseId": this.data.allCourse[e.detail.value].id
    })
  },
  selectTeacher(e) {
    //选择老师
    this.setData({
      selTeacherIndex: e.detail.value,
      "curPostData.teacherNum": this.data.allTeacher[e.detail.value].id
    })
  },
  DateChange(e) {
    //日期选择
    this.setData({
      date: e.detail.value
    })
  },
  TimeChange(e) {
    //日期选择
    this.setData({
      time: e.detail.value
    })
  },
  hideModalAdd() {
    this.setData({
      showModalAdd: false
    })
  },
  getAllTeacher() {
    //获取所有老师    
    wx.request({
      url: app.globalData.baseService + "/user/selAll",
      success: (res) => {
        if (res.data.errorCode == 0) {
          let oData = res.data.data.filter(item => item.roleId == 2)
          this.setData({
            allTeacher: oData
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  getAllCollege() {
    //获取所有学院
    wx.request({
      url: app.globalData.baseService + "/college/selAll",
      success: (res) => {
        if (res.data.errorCode == 0) {
          this.setData({
            collegeData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  getSpeByCollege(id) {
    wx.request({
      url: app.globalData.baseService + "/specialty/selAllUnderOneSpecialty",
      data: {
        collegeId: id
      },
      success: (res) => {
        if (res.data.errorCode == 0) {
          this.setData({
            specialtyData: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  getAllCourse() {
    //获取所有课程
    wx.request({
      url: app.globalData.baseService + '/course/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.setData({
            allCourse: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  getCurDate() {
    let date = new Date();
    let year = date.getFullYear(),
      month = (+date.getMonth()) > 8 ? +date.getMonth() + 1 : "0" + (+date.getMonth() + 1),
      day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
    this.setData({
      examStartDate: year + '-' + month + '-' + day
    })
  },
  addCourse() {
    this.setData({
      showModalAdd: true,
      selCourseIndex: null,
      selTeacherIndex: null,
    })
  },
  confirmEnter() {
    console.log(this.data.curPostData);
    let yearArr = this.data.yearsData[this.data.yearIndex].arr;
    let dataset = { ...this.data.curPostData,
      rebuildTime: this.data.date + " " + this.data.time + ":00",
      startYear: yearArr[0],
      endYear: yearArr[1],
      term: yearArr[2]
    };
    wx.request({
      url: app.globalData.baseService + "/rebuild/add", //http://www.hotant.top
      method: 'POST',
      data: dataset,
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          //更新列表
          this.getResultsByCondition();
          this.hideModalAdd();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  getResultsByCondition(cb) {
    let yearArr = this.data.yearsData[this.data.yearIndex].arr;
    let dataset = {
      startYear: yearArr[0],
      endYear: yearArr[1],
      term: yearArr[2]
    }
    wx.request({
      url: app.globalData.baseService + "/rebuild/selByConditon", //http://www.hotant.top
      data: dataset,
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          //更新列表
          this.setData({
            allListData: res.data.data
          })
          if (this.data.specialtyIndex) {
            let filterData = res.data.data.filter(item =>item.student.specialtyName == this.data.specialtyData[this.data.specialtyIndex].specialtyName);
            this.setData({
              listData: filterData
            })
          }
          cb && cb(res.data.data)
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  toReadMore(e) {
    let itemData = e.currentTarget.dataset.item;
    this.setData({
      showModal: true,
      detailData: itemData
    })
  },
  hideModal() {
    this.setData({
      showModal: false,
      detailData: {}
    })
  },
  toDel(e) {
    this.setData({
      showModalDel: true,
      delId: e.currentTarget.dataset.item
    })
  },
  hideModalDel() {
    this.setData({
      showModalDel: false
    })
  },
  confirmDel() {
    //确认删除
    let delId = this.data.delId;
    wx.request({
      url: app.globalData.baseService + '/rebuild/del',
      data: {
        id: delId
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          wx.showToast({
            title: "删除成功",
            icon: 'success',
            duration: 2000
          })
          this.getResultsByCondition();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        this.hideModalDel();
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})