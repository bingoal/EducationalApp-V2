//index.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js')

Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    loadModal:false,
    userInfo: null,
    userLogined: null,
    hasUserInfo: false,
    localuser:{num:null,pwd:null},
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    roleIndex: null,
    picker: [{ name: "学员", roleId: 3 }, { name: "教师", roleId: 2 }, { name: "教务管理员", roleId: 1 }],//角色
    allNotice:[],
    noticeModal:false,//弹框
    curNotice:null
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onGotUserInfo: function (e) {
    console.log(e)
    if (e.detail.userInfo) {
      app.globalData.userInfo = e.detail.userInfo;
      this.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true
      })
    }
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  PickerChange(e) {
    this.setData({
      roleIndex: e.detail.value,
    })
  },
  loginLocal(){
    //本地登录
    // if(!roleIndex){
    //   wx.showToast({
    //     title: '请选择角色',
    //     icon: 'success',
    //     duration: 2000
    //   });
    //   return false;
    // }
    let dataSet = { num: this.data.localuser.num, password: this.data.localuser.password}
    wx.request({
      url: app.globalData.baseService + '/user/login',
      method: "POST",
      data: dataSet,
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:(res)=>{
        console.log("用户登录返回", res);
        if (res.data.errorCode == 0) {
          // app.globalData.roleId = res.data.data.roleId;
          // _this.setData({
          //   roleId: res.data.data.roleId
          // })
          wx.showToast({
            title: '登录成功',
            icon: 'success',
            duration: 2000
          });
          this.setData({
            userLogined: res.data.data.id
          })
          wx.setStorage({
            key: "userInfo",
            data: res.data.data
          })

          app.globalData.userId = res.data.data.id;
          app.globalData.roleId = res.data.data.roleId;
          app.globalData.localuser = res.data.data
        }else{
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    });
  },
  getNotices(){
    wx.request({
      url: app.globalData.baseService + '/notice/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          res.data.data.map((item) => {
            item.otime = util.formatTimeThree(item.createTime)
          })
          this.setData({
            allNotice: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        // this.hideModal();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  showNoticeInfo(e){
    this.setData({
      noticeModal:true,
      curNotice: e.currentTarget.dataset.item
    })
  },
  hideModal(){
    this.setData({
      noticeModal: false,
      curNotice: null
    })
  },
  onLoad: function () {
    // app.getOpenid((data)=>{
    //   this.setData({
    //     loadModal: false
    //   })
    //   this.setData({
    //     userLogined:data.roleId
    //   })
    // }, () => {
    //   this.setData({
    //     loadModal: false
    //   })})
    
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  onShow: function () {
    //监听页面显示
    this.getNotices();
    console.log(app.globalData.userId)
    this.setData({
      userLogined: app.globalData.userId
    })
    // wx.getStorage({
    //   key: 'userInfo',
    //   success: (res) => {
    //     this.setData({
    //       userLogined: Boolean(app.globalData.userId)
    //     })
    //   },
    // })
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
