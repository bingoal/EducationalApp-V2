// pages/myTimeTable/myTimeTable.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    requestUrl: ['/college', '/specialty', '/clazz'],
    textName: ['学院', '专业', '班级'],
    pickerIndex: null,
    allCollege: [],
    allSpecialty: [],
    allClass: [],
    tabCur: 0,
    activeDay: 0,
    classData: [],
    showModal: false,
    showModalDel: false,
    isAddCourse: false,
    curCourseData: {},
    index: null,
    delId: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options);
    this.getAllClass();
    this.getAllSpe();
  },
  // function
  tabSelect(e) {
    let tempData = e.currentTarget.dataset.id;
    if (this.data.tabCur == tempData) return false;
    this.setData({
      tabCur: tempData
    })
    this.getAllClass();
  },
  getAllSpe() {
    //获取所有专业
    wx.request({
      url: app.globalData.baseService + "/specialty/selAll",
      success: (res) => {
        if (res.data.errorCode == 0) {
          this.setData({
            allSpecialty: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  getAllClass() {
    //获取所有院系或者班级
    wx.request({
      url: app.globalData.baseService + this.data.requestUrl[this.data.tabCur] + '/selAll',
      success: (res) => {
        console.log("结果", res);
        let objKey = this.data.tabCur == 0 ? "allCollege" : (this.data.tabCur == 1 ? "allSpecialty" : "allClass")
        if (res.data.errorCode == 0) {
          this.setData({
            [objKey]: res.data.data
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  collegeChange(e) {
    console.log(e);
    let objKey = (this.data.tabCur && this.data.tabCur == 1) ? "curCourseData.collegeId" : "curCourseData.specialtyId";
    this.setData({
      pickerIndex: e.detail.value,
      [objKey]: e.detail.value
    });
  },
  toDeleteCourse(e) {
    // 删除班级
    this.setData({
      showModalDel: true,
      delId: e.currentTarget.dataset.delid
    })
  },
  toEditCourse(e) {
    //编辑用户信息
    console.log(e)
    let key = ["collegeName", "specialtyName", "clazzName"];
    let tempObj = JSON.parse(JSON.stringify(e.currentTarget.dataset.course));
    tempObj.clazzName = tempObj[key[this.data.tabCur]];
    this.data.allCollege.forEach((item, index) => {
      if (this.data.tabCur == 1 && tempObj.collegeId == item.id) {
        this.setData({
          pickerIndex: index + ""
        })
      }
    })
    this.data.allSpecialty.forEach((item, index) => {
      if (this.data.tabCur == 2 && tempObj.specialtyId == item.id) {
        this.setData({
          pickerIndex: index + ""
        })
      }
    })
    console.log(tempObj)
    this.setData({
      showModal: true,
      isAddCourse: false,
      curCourseData: tempObj
    })
  },
  addCourse() {
    // 新增用户
    this.setData({
      showModal: true,
      isAddCourse: true
    })
  },
  PickerChange(e) {
    console.log(e.detail.value)
    this.setData({
      index: e.detail.value,
      "curCourseData.name": e.detail.value
    })
  },
  hideModal() {
    this.setData({
      showModal: false,
      curCourseData: {}
    })
  },
  hideModalDel() {
    this.setData({
      showModalDel: false
    })
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  confirmEnter() {
    //确认编辑
    console.log(this.data.curCourseData)
    let dataset = {};
    if (!this.data.isAddCourse) {
      dataset = JSON.parse(JSON.stringify(this.data.curCourseData));
    }
    let tempKey = ["collegeName", "specialtyName", "clazzName"][this.data.tabCur];
    dataset[tempKey] = this.data.curCourseData.clazzName;
    let parentIndex = this.data.pickerIndex;
    if (!parentIndex) {
      return false;
    }
    switch (this.data.tabCur) {
      case 1:
        dataset.collegeId = this.data.allCollege[parentIndex].id;
        break;
      case 2:
        dataset.specialtyId = this.data.allSpecialty[parentIndex].id
        break;
    }
    let funcApi = this.data.isAddCourse ? "/add" : "/upd";
    wx.request({
      url: app.globalData.baseService + this.data.requestUrl[this.data.tabCur] + funcApi,
      method: 'POST',
      data: dataset,
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        if (res.data.errorCode == 0) {
          let dataName = ['allCollege', 'allSpecialty'];
          this.setData({
            [dataName]: res.data.data
          })
          this.getAllClass()
          this.hideModal();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  confirmDel() {
    //确认删除
    console.log(this.data.delId);
    wx.request({
      url: app.globalData.baseService + this.data.requestUrl[this.data.tabCur] + '/del',
      data: {
        id: this.data.delId
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.getAllClass()
          this.hideModalDel();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})