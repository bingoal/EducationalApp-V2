// pages/vocationManage/vocationManage.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    allData:[],
    showModal:false,
    showModalDel:false,
    curCourseData: {},
    startDate: null,
    startTime: null,
    endTime:null,
    delId:null
  },
  // methods
  getAllVocation(){
    //获取所有假期
    wx.request({
      url: app.globalData.baseService + '/vacation/selAll',
      success: (res) => {
        console.log("结果", res);
        if (res.data.errorCode == 0) {
          let oRes = res.data.data;
          this.setData({
            allData: oRes
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
    })
  },
  addCourse() {
    // 新增
    this.setData({
      showModal: true,
      startTime: null,
      endTime:null
    })
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  DateChange(e) {
    let key =e.currentTarget.dataset.name,
      objKey="curCourseData." + key;
    console.log(objKey)
    //日期选择
    this.setData({
      [key]: e.detail.value,
      [objKey]: e.detail.value + " 00:00:00"
    })
    console.log(this.data.endTime)
  },
  confirmEnter() {
    //确认编辑
    console.log(this.data.curCourseData);
    wx.request({
      url: app.globalData.baseService+'/vacation/add',
      method:"post",
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      data: this.data.curCourseData,
      success: (res) => {
        console.log("拿到的res", res);
        if (res.data.errorCode == 0) {
          //更新列表
          this.getAllVocation();
          this.hideModal();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  hideModal() {
    this.setData({
      showModal: false,
      curCourseData: {}
    })
  },
  toDelItem(e) {
    // 删除班级
    this.setData({
      showModalDel: true,
      delId: e.currentTarget.dataset.delid
    })
  },
  confirmDel() {
    //确认删除
    console.log(this.data.delId)
    wx.request({
      url: app.globalData.baseService + '/vacation/del',
      data: { id: this.data.delId },
      success: (res) => {
        console.log("拿到的res", res);
        if (res.data.errorCode == 0) {
          wx.showToast({
            title: "删除成功",
            icon: 'success',
            duration: 2000
          })
          this.getAllVocation();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        this.hideModalDel();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  hideModalDel() {
    this.setData({
      showModalDel: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAllVocation();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})