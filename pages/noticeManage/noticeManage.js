// pages/teaTaskManage/teaTaskManage.js
const app=getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    courseData: [],
    showModal: false,
    showModalDel:false,
    curCourseData: { courseStatus:1},
    index:null,
    statusText:["必修课","选修课"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getAllCourse();
  },
  // functions
  getAllCourse(){
    //获取所有课程
    wx.request({
      url: app.globalData.baseService + '/notice/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode==0) {
          res.data.data.map((item)=>{
            item.otime = util.formatTimeThree(item.createTime)
          })
          console.log(res.data.data)
            this.setData({
              courseData:res.data.data
            })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  toDelCourse(e) {
    //编辑用户信息
    console.log(e)
    let tempObj = JSON.parse(JSON.stringify(e.currentTarget.dataset.course));
    this.setData({
      showModalDel: true,
      curCourseData: tempObj
    })
  },
  confirmDel() {
    //确认删除
    let delId = this.data.curCourseData.id;
    wx.request({
      url: app.globalData.baseService + '/notice/del',
      data: { id: delId },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          wx.showToast({
            title: "删除成功",
            icon: 'success',
            duration: 2000
          })
          this.getAllCourse();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        this.hideModalDel();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  addCourse() {
    // 新增用户
    this.setData({
      showModal: true
    })
  },
  hideModal() {
    this.setData({
      showModal: false,
      curCourseData: {}
    })
  },
  hideModalDel() {
    this.setData({
      showModalDel: false,
      curCourseData: {}
    })
  },
  getInputVal(e) {
    //获取失焦后输入框的值
    let name = e.currentTarget.dataset.name;
    let nameMap = {}
    nameMap[name] = e.detail && e.detail.value
    this.setData(nameMap)
  },
  confirmEnter() {
    //确认编辑
    console.log(this.data.curCourseData)
    wx.request({
      url: app.globalData.baseService + '/notice/add', //http://www.hotant.top
      method: 'POST',
      data: this.data.curCourseData,
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          this.getAllCourse();
          this.hideModal();
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})