// pages/examList/examList.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    TabCur:0,
    lengthArr:[0,0],
    examType:[],
    eduData:[],
    examDetailData: [],//{ id: 1, courseName: "语文期末考试", time: "2020-2-13 16:00", site: "第五教学楼5102室", teacher: "李文旭", }, { id: 2, courseName: "英语期末考试", time: "2020-2-12 16:00", site: "第三教学楼5102室", teacher: "张文旭" }, { id: 3, courseName: "高等数学结业考试", time: "2020-2-13 8:00", site: "第五教学楼5102室", teacher: "赵文旭" }
    selectEduIndex:null,//教务索引
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAllExam();
    this.setData({
      examType: app.globalData.examType
    })
  },
// functions
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id
    })
    this.getAllExam();
  },
  eduChange(e){
    this.setData({
      "selectEduIndex": e.detail.value
    })
    this.getExamDetail();
  },
  getAllExam() {
    wx.request({
      url: app.globalData.baseService + '/exam/selAll',
      success: (res) => {
        console.log("拿到的sessionkey", res);
        if (res.data.errorCode == 0) {
          // this.getAllEdu();
          let oRes = res.data.data || [];
          let tempArr = [0, 0];
          let oData = oRes.filter(item => item.clazzId == app.globalData.localuser.clazzId)
          oData.filter(item => {
            tempArr[item.flag] += 1;
          })
          let readyData = oData.filter(item => item.flag == this.data.TabCur);
          this.setData({
            "examDetailData": readyData,
            "lengthArr": tempArr
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
        // this.hideModal();
      },
      fail() {
        wx.showToast({
          title: "请求出错，请稍后重试！",
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})