// pages/calendar/calendar.js
//是否当年为闰年
// let isLeapYear=(year)=>{
//   return (year%400===0 || year%4===0&&year%100!==0)?1:0;
// }
// //公历月份天数
// const DayOfMonth = [
//   [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
//   [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
// ];
// //获取当月有多少天
// let getDayCount=(year,month)=> DayOfMonth[isLeapYear(year)][month];

// let pageData={
//   curDate:"",//当期日期
//   termDate:"2019-2020-1",
//   weekNameArr: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"]
// }
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    curDate:{},
    allVocation:[],
    termDate:"2019-2020-1",
    termStart:"2020-09-21",
    vocationName:"",
    bigVocation:{}
  },
  // methods
  getCurDateInfo(){
    //根据当前时间，获取要素
    let date=new Date();
    let year=date.getFullYear(),
        month=date.getMonth()+1;
    let term = month >= 2&&month<=9 ? 2 :1,
      termYear = term == 1 ? year + "-" + year + 1 : year-1 + "-" + year;
    let dateObj = { year, month, term,termYear}
    this.setData({
      curDate:dateObj,
      termDate: termYear+"-"+term
    })
  },
  getAllVocation() {
    //获取所有假期
    wx.request({
      url: app.globalData.baseService + '/vacation/selAll',
      success: (res) => {
        console.log("结果", res);
        if (res.data.errorCode == 0) {
          let oRes = res.data.data;
          let tempFlagYear = (this.data.curDate.term == 1 ? this.data.curDate.year + 1 : this.data.curDate.year),
            tempFlagName = this.data.curDate.term == 1 ? "寒假" : "暑假",
          tempFlag = tempFlagYear + tempFlagName;
          this.setData({
            vocationName: tempFlagName
          })
          oRes.forEach(item=>{
            console.log(item)
            if (item.vacationName == tempFlagYear+"开学"){
              this.setData({
                termStart: item.startTime.substr(0, 10)
              })
            }
            if (item.vacationName == tempFlag){
              let tempObj = { start: item.startTime.substr(0, 10), end: item.endTime.substr(0, 10)}
              this.setData({
                bigVocation: tempObj
              })
            }
            
          })
          this.setData({
            allVocation: oRes
          })
        } else {
          wx.showToast({
            title: res.data.errorMsg,
            icon: 'none',
            duration: 2000
          })
        }
      },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getCurDateInfo();
    this.getAllVocation();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})